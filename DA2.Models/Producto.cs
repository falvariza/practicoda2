﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA2.Models
{
    [Table("Productos")]
    public class Producto : Entidad
    {
        public virtual List<Imagen> Imagenes { get; private set; }
        public double Precio { get; set; }
        public string Nombre { get; set; }
        public virtual Video Video { get; set; }
        public string Descripcion { get; set; }
        public virtual List<Caracteristica> Caracteristicas { get; private set; }

        public Producto()
        {
            this.Imagenes = new List<Imagen>();
            this.Caracteristicas = new List<Caracteristica>();
        }
    }
}
