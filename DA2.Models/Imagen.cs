﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DA2.Models
{
    [Table("Imagenes")]
    public class Imagen : Entidad
    {
        public string Url { get; set; }
    }
}
