﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DA2.Models
{
    [Table("Caracteristicas")]
    public class Caracteristica : Entidad
    {
        public string Nombre { get; set; }
        public string Valor { get; set; }
    }
}
