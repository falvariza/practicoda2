﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA2.Models
{
    [Table("Usuarios")]
    public class Usuario : Entidad
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public virtual Direccion Direcion { get; set; }
        public string Email { get; set; }
        public string Contraseña { get; set; }
        public virtual Carrito Carrito { get; set; }
    }
}
