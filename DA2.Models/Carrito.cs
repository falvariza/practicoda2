﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA2.Models
{
    [Table("Carritos")]
    public class Carrito : Entidad
    {
        public virtual List<LineaProducto> Productos { get; private set; }

        public Carrito()
        {
            this.Productos = new List<LineaProducto>();
        }
    }
}
