﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DA2.Models
{
    [Table("Direcciones")]
    public class Direccion : Entidad
    {
        public string Calle { get; set; }
        public string NumeroPuerta { get; set; }
        public int NumeroApartamento { get; set; }
        public string Estado { get; set; }
        public string Pais { get; set; }
        public string Telefono { get; set; }
    }
}
