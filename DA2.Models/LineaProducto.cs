﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DA2.Models
{
    public class LineaProducto : Entidad
    {
        public virtual Producto Producto { get; set; }
        public int Cantidad { get; set; }
    }
}
