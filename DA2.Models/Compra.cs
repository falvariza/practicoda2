﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA2.Models
{
    [Table("Compras")]
    public class Compra : Entidad
    {
        public virtual Usuario Usuario { get; set; }
        public virtual List<LineaProducto> Productos { get; set; }

        public Compra()
        {
            this.Productos = new List<LineaProducto>();
        }
    }
}
