﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA2.Models
{
    public class DataAccess : IDisposable
    {
        private ECommerceContext context;

        public DataAccess()
        {
            this.context = new ECommerceContext();
        }

        public T Create<T>(T entidad) where T : Entidad
        {
            entidad.Created = DateTime.Now;
            entidad.Updated = entidad.Created;
            this.context.Set<T>().Add(entidad);
            this.context.SaveChanges();
            return entidad;
        }

        public void Dispose()
        {
            this.context.Dispose();
        }
    }
}
