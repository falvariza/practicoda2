﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DA2.Models
{
    [Table("Videos")]
    public class Video : Entidad
    {
        public string IdYoutube { get; set; }

        public string Url
        {
            get
            {
                return "http:://www.youtube.com/" + this.IdYoutube;
            }
        }
    }
}
