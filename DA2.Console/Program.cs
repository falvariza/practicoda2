﻿using DA2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA2.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new DataAccess())
            {
                Producto p1 = new Producto() { Nombre = "Producto 1", Precio = 123, Video = new Video() { IdYoutube = "456" } };
                db.Create(p1);
            }
        }
    }
}
