﻿using DA2.Models;
using DA2.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DA2.Mvc.Controllers
{
    public class ProductosController : ApiController
    {
        // GET api/productos
        public IEnumerable<ProductoDTO> Get(string filtro = null)
        {
            IQueryable<Producto>  listadoProductos = new ECommerceContext().Productos;

            if (!string.IsNullOrEmpty(filtro))
            {
                listadoProductos=listadoProductos.Where(p => p.Nombre == filtro);
            }

            return listadoProductos.Select(
                p => new ProductoDTO() { Id = p.ID, Nombre = p.Nombre });
        }

        // GET api/productos/5
        public ProductoDTO Get(int id)
        {
            Producto p = new ECommerceContext().Productos.Find(id);

            return new ProductoDTO() { Id = p.ID, Nombre = p.Nombre };
        }

        // POST api/productos
        public void Post([FromBody]ProductoDTO p)
        {

        //    var context = new ECommerceContext();
            //context.Productos.Add(new Producto() { Nombre = value.Nombre });
        }

        // PUT api/productos/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/productos/5
        public void Delete(int id)
        {
        }
    }
}
