﻿(function () {
	'use strict';
	angular
		.module('App')
		.controller('Items.Controller', ItemController);
	function ItemsController($scope) {
		$scope.items = [{ ID: 1, Name: 'Shirt', Price: 20 }, { ID: 2, Name: 'Pants', Price: 30 }];
		$scope.formatPrice = function formatPrice(item) {
			return '$' + item.Price + '.00';
		}
	}
})();